# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from erpnext.setup.doctype.customer_group.customer_group import CustomerGroup
import frappe.www.printview
from .utils.printpreview import _get_print_format

__version__ = '0.0.1'

if not hasattr(CustomerGroup, 'status_data'):
	from .trademarks.print_format.status_report.status_report import status_data
	setattr(CustomerGroup, 'status_data', status_data)

frappe.www.printview.get_print_format = _get_print_format