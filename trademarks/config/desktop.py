# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Trademarks",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Trademarks")
		},
		{
			"module_name": "Trademark map",
			"color": "green",
			"icon": "fa fa-globe",
			"type": "page",
			"link": "trademark-map",
			"label": _("Trademark map")
		}
	]
