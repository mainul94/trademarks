from __future__ import unicode_literals
from frappe import _


def get_data():
	return [
		{
			"label": _("Documents"),
			"items": [
				{
					"type": "doctype",
					"name": "Trademark Classes",
					"doctype": "Trademark Classes",
					"label": _("Classes")
				},
				{
					"type": "doctype",
					"name": "Trademark",
					"doctype": "Trademark",
					"label": _("Trademark")
				}
			]
		},
		{
			"label": _("Setup"),
			"items": [
				{
					"type": "doctype",
					"name": "Trademark Classes Template",
					"doctype": "Trademark Classes Template",
					"label": _("Classes List")
				},
				{
					"type": "doctype",
					"name": "Trademarks Status",
					"doctype": "Trademarks Status",
					"label": _("Status")
				},
				{
					"type": "doctype",
					"name": "Documents",
					"doctype": "Documents",
					"label": _("Document")
				},
				{
					"type": "doctype",
					"name": "Require Document Setup",
					"doctype": "Require Document Setup",
					"label": _("Require Document Setup")
				}
			]
		}
    ]