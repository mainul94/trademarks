frappe.provide('trademark')

trademark.DocumentEditor = Class.extend({
	init: function(wrapper, frm) {
		this.frm = frm;
		this.wrapper = wrapper;
		$(wrapper).html('<div class="help">' + __("Loading") + '...</div>');
		return this.fetch_documents()
	},
	fetch_documents: function () {
		let me = this;
		if (!(me.frm.doc.region && me.frm.doc.company_type)) {
			if (me.frm.documents_editor)
				me.frm.documents_editor.show();
			return
		}
		return frappe.call({
			method: 'trademarks.trademarks.doctype.require_document_setup.require_document_setup.get_documents',
			args: {
				region: me.frm.doc.region,
				company_type: me.frm.doc.company_type
			},
			callback: function(r) {
				me.documents = r.message;
				me.show_documents();
				// refresh call could've already happened
				// when all document checkboxes weren't created
				if(me.frm.doc) {
					me.frm.documents_editor.show();
				}
			}
		});
	},
	show_documents: function() {
		var me = this;
		$(this.wrapper).empty();
		$.each(this.documents, function(i, document) {
			$(me.wrapper).append(repl('<div class="user-document" \
				data-user-document="%(document_value)s">\
				<input type="checkbox" style="margin-top:0px;" class="box"> \
				<label class="grey document">%(document_display)s</label>\
			</div>', {document_value: document,document_display:__(document)}));
		});

		$(this.wrapper).find('input[type="checkbox"]').change(function() {
			me.set_documents_in_table();
			me.frm.dirty();
		});
	},
	show: function() {
		var me = this;

		// uncheck all documents
		$(this.wrapper).find('input[type="checkbox"]')
			.each(function(i, checkbox) { checkbox.checked = false; });

		// set user documents as checked
		$.each((me.frm.doc.documents || []), function(i, user_document) {
			var checkbox = $(me.wrapper)
				.find('[data-user-document="'+user_document.document+'"] input[type="checkbox"]').get(0);
			if(checkbox) checkbox.checked = true;
		});

	},
	set_documents_in_table: function() {
		var opts = this.get_documents();
		var existing_documents_map = {};
		var existing_documents_list = [];
		var me = this;

		$.each((me.frm.doc.documents || []), function(i, user_document) {
			existing_documents_map[user_document.document] = user_document.name;
			existing_documents_list.push(user_document.document);
		});

		// remove unchecked documents
		$.each(opts.unchecked_documents, function(i, document) {
			if(existing_documents_list.indexOf(document)!=-1) {
				frappe.model.clear_doc("Has Document", existing_documents_map[document]);
			}
		});

		// add new documents that are checked
		$.each(opts.checked_documents, function(i, document) {
			if(existing_documents_list.indexOf(document)==-1) {
				var user_document = frappe.model.add_child(me.frm.doc, "Has Document", "documents");
				user_document.document = document;
			}
		});

		refresh_field("documents");
	},
	get_documents: function() {
		var checked_documents = [];
		var unchecked_documents = [];
		$(this.wrapper).find('[data-user-document]').each(function() {
			if($(this).find('input[type="checkbox"]:checked').length) {
				checked_documents.push($(this).attr('data-user-document'));
			} else {
				unchecked_documents.push($(this).attr('data-user-document'));
			}
		});

		return {
			checked_documents: checked_documents,
			unchecked_documents: unchecked_documents
		};
	}
});

frappe.ui.form.Attachments = frappe.ui.form.Attachments.extend({
	get_args: function () {
		let args = this._super()
		args['is_private'] = 1
		return args
	}
})
comment_when = function (datetime, mini) {
	var timestamp = frappe.datetime.str_to_user ?
		frappe.datetime.str_to_user(datetime) : datetime;
	return '<span class="frappe-timestamp '
		+ (mini ? " mini" : "") + '" data-timestamp="' + datetime
		+ '" title="' + timestamp + '">'
		+ (cur_frm && ['Trademark'].includes(cur_frm.doctype)? timestamp: prettyDate(datetime, mini)) + '</span>';
};