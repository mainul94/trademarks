frappe.ui.form.PrintPreview = frappe.ui.form.PrintPreview.extend({
    bind_events: function () {
        this._super()
        let me = this;
        if (['Trademark'].includes(me.frm.doctype)) {
            me.wrapper.find('.btn-print-print, .btn-download-pdf').on('click', function () {
                let purpose = $(this).hasClass('btn-print-print') ? "Print" : "Download PDF"
                let doc = {
                    ref_doctype: me.frm.doctype,
                    docname: me.frm.doc.name,
                    data: JSON.stringify({
                        comment_type: "Workflow",
                        comment: purpose + ' ' + me.selected_format()
                    }),
                    doctype: "Version"
                }
                frappe.call({
                    method: "frappe.client.insert",
                    args: {
                        doc: doc
                    }
                })
            })
        }
    }
})