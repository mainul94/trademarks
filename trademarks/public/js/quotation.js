frappe.ui.form.on("Quotation", {
    setup: function(frm) {
        frm.add_fetch('item_code', 'application_for_1st_class', 'application_for_1st_class')
        frm.add_fetch('item_code', 'application_for_each_additional_class', 'application_for_each_additional_class')
        frm.add_fetch('item_code', 'registration_for_1st_class', 'registration_for_1st_class')
        frm.add_fetch('item_code', 'registration_for_each_additional_class', 'registration_for_each_additional_class')
        frm.add_fetch('item_code', 'region', 'region')
        frm.add_fetch('item_code', 'notarization_fee', 'notarization_fee')
        frm.add_fetch('item_code', 'protection_period', 'protection_period')
        frm.add_fetch('item_code', 'application_time', 'application_time')
    }
})