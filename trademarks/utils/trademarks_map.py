# coding=utf-8
from __future__ import unicode_literals
import frappe
import json
from erpnext.controllers.queries import get_match_cond
from six import string_types


@frappe.whitelist()
def get_data(filters=None):
    if not filters:
        filters = {}
    if isinstance(filters, string_types):
        filters = json.loads(filters)
    conditions = prepare_conditions(filters, 'name')
    if filters.get('trademark_class') and not filters.get('trademark'):
        trademarks = [x.trademark for x in frappe.get_all('Trademark Classes', {'trademark': filters.get('trademark')}, 'trademark')]
        conditions += " and name in ('{}')".format("', '".join(trademarks))

    return frappe.db.sql("""select region, count(name) from tabTrademark
    where region is not null and status in ('Registered', 'Wait for renewal', 'Wait for certificate') {} group by region""".format(conditions))


def prepare_conditions(filters, tr_field_name='trademark'):
    filters = frappe._dict(filters)
    conditions = []

    if filters.trademark:
        conditions.append("{}='{}'".format(tr_field_name, filters.trademark))
    elif filters.customer:
        conditions.append("customer='{}'".format(filters.customer))
    elif filters.get('customer_group'):
        lft, rgt = frappe.db.get_value("Customer Group",
            filters.get("customer_group"), ["lft", "rgt"])

        conditions.append("""customer in (select name from tabCustomer
            where exists(select name from `tabCustomer Group` where lft >= {0} and rgt <= {1}
                and name=tabCustomer.customer_group))""".format(lft, rgt))

    return  " and " + " and ".join(conditions) if conditions else ""


@frappe.whitelist()
def get_trademark(doctype, txt, searchfield, start, page_len, filters):
    conditions = prepare_conditions(filters)
    return frappe.db.sql("""select name, project_name, customer, status, region from `tabTrademark`
    	where tabTrademark.status in ('Registered', 'Wait for renewal', 'Wait for certificate')
    		and tabTrademark.{key} LIKE %(txt)s
    		{condition} {match_condition}"""
                         .format(condition=conditions, key=frappe.db.escape(searchfield),
                                 match_condition=get_match_cond(doctype)), {'txt': "%%%s%%" % frappe.db.escape(txt)
                         })


@frappe.whitelist()
def get_trademark_class(doctype, txt, searchfield, start, page_len, filters):
    conditions = prepare_conditions(filters)
    return frappe.db.sql("""select tct.name from `tabTrademark Classes Template` as tct
        right join `tabTrademark Classes` as tc on tc.trademark_class = tct.name
        left join `tabTrademark` as t on t.name = tc.trademark
    	where tc.class_status = 'Registered'
    		and tct.{key} LIKE %(txt)s
    		{condition} {match_condition}"""
                         .format(condition=conditions, key=frappe.db.escape(searchfield),
                                 match_condition=get_match_cond(doctype)), {'txt': "%%%s%%" % frappe.db.escape(txt)
                         })
