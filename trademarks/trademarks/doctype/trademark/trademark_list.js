frappe.provide('trademark')
if (typeof trademark.status === 'undefined') {
	trademark.status = {}
	frappe.db.get_list('Trademarks Status', {
		limit: 0,
		fields: ['name', 'color']
	}).then( resoved => {
		resoved.map(function(d){
			trademark.status[d.name] = d.color
		})
	})
}
frappe.listview_settings['Trademark'] = {
	add_fields: ["status"],
	get_indicator: function(doc) {
		return [__(doc.status), trademark.status[doc.status], "status,=," + doc.status];
	},
	post_render_item: (list, $item_container, value) => {
			$item_container.find('a[data-name]').closest('.list-item__content.ellipsis').removeClass('list-item__content--flex-2')
			$item_container.find('span[data-filter^="status"]').closest('.list-item__content.ellipsis').addClass('list-item__content--flex-2').attr('title', value.status)
	}
};
