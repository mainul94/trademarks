// Copyright (c) 2018, ahmedragheb75@gmail.com and contributors
// For license Trademark, please see license.txt

{% include 'trademarks/public/js/require_documents.js' %};

frappe.ui.form.on("Trademark", {
	setup: function (frm) {
		frm.add_fetch('trademark_class', 'default_description', 'trademark_class_description')
		frm.add_fetch('customer', 'customer_type', 'company_type')
		let img_fields = ['image', 'trademark_image_1', 'trademark_image_2','trademark_image_3','trademark_image_4'];
		for (let x =0; x < img_fields.length; x++) {
			frm.set_df_property(img_fields[x], 'is_private', true)
		}
	},

	onload: function (frm) {
		var so = frappe.meta.get_docfield("Trademark", "sales_order");
		so.get_route_options_for_new_doc = function (field) {
			if (frm.is_new()) return;
			return {
				"customer": frm.doc.customer,
				"project_name": frm.doc.name
			}
		}

		frm.set_query('customer', 'erpnext.controllers.queries.customer_query');

		frm.set_query("user", "users", function () {
			return {
				query: "trademarks.trademarks.doctype.trademark.trademarks.get_users_for_project"
			}
		});

		// sales order
		frm.set_query('sales_order', function () {
			var filters = {
				'project': ["in", frm.doc.__islocal ? [""] : [frm.doc.name, ""]]
			};

			if (frm.doc.customer) {
				filters["customer"] = frm.doc.customer;
			}

			return {
				filters: filters
			}
		});

		// Require Documents
		if(!frm.documents_editor) {
			var document_area = $('<div style="min-height: 300px">')
				.appendTo(frm.fields_dict.document_html.wrapper);
			frm.documents_editor = new trademark.DocumentEditor(document_area, frm);
		} else {
			frm.documents_editor.fetch_documents();
		}
	},
	region: function (frm) {
		if(frm.doc.region && frm.doc.company_type)
		frm.documents_editor.fetch_documents();
	},
	company_type: function (frm) {
		if(frm.doc.region && frm.doc.company_type)
		frm.documents_editor.fetch_documents();
	},
	refresh: function (frm) {
		if (frm.doc.__islocal) {
			frm.web_link && frm.web_link.remove();
		} else {
			frm.add_web_link("/trademarks?trademark=" + encodeURIComponent(frm.doc.name));

			if (frappe.model.can_read("Task")) {
				frm.add_custom_button(__("Gantt Chart"), function () {
					frappe.route_options = { "trademark": frm.doc.name };
					frappe.set_route("List", "Task", "Gantt");
				});
			}

			frm.trigger('show_dashboard');
		}
		// Status
		frm.trigger('set_status_options')

		if(frm.doc.region && frm.doc.company_type)
			frm.documents_editor.fetch_documents();
	},
	region: function (frm) {
		// Status
		frm.trigger('set_status_options')
	},
	set_status_options: function (frm) {
		let old_status = frm.doc.status || '';
		if (frm.doc.__islocal) {
			doctype = 'Trademarks Status'
			filters = { 'is_root': 1 }
		} else {
			doctype = 'Trademarks Status Child'
			filters = { 'parent': frm.doc.status }
		}
		if (!frm.doc.region) {
			set_field_options('status', [])
			return
		} else {
			filters['territory'] = ['like', '%' + frm.doc.region + '%']
		}
		filters['for_trademark'] = 1
		frappe.call({
			method: 'frappe.client.get_list',
			args: {
				doctype: doctype,
				limit_page_length: 0,
				fields: doctype == "Trademarks Status" ? 'name' : 'trademarks_status',
				filters: filters,
				parent: doctype == 'Trademarks Status Child' ? 'Trademarks Status' : '',
				order_by: 'idx asc'
			},
			callback: r => {
				let options = []
				if (r['message']) {
					options = $.map(r.message, function (d) {
						return doctype == 'Trademarks Status' ? d.name : d.trademarks_status
					})
				}
				if (!frm.doc.__islocal)
					options.unshift(old_status);
				set_field_options('status', options)
			}
		})
	},
	show_dashboard: function (frm) {
		if (frm.doc.__onload.activity_summary.length) {
			var hours = $.map(frm.doc.__onload.activity_summary, function (d) { return d.total_hours });
			var max_count = Math.max.apply(null, hours);
			var sum = hours.reduce(function (a, b) { return a + b; }, 0);
			var section = frm.dashboard.add_section(
				frappe.render_template('project_dashboard',
					{
						data: frm.doc.__onload.activity_summary,
						max_count: max_count,
						sum: sum
					}));

			section.on('click', '.time-sheet-link', function () {
				var activity_type = $(this).attr('data-activity_type');
				frappe.set_route('List', 'Timesheet',
					{ 'activity_type': activity_type, 'project': frm.doc.name, 'status': ["!=", "Cancelled"] });
			});
		}
	}
});


frappe.ui.form.on("Trademark Child Classes", {
	edit_class: function(frm, cdt, cdn) {
		let d = frappe.get_doc(cdt, cdn)
		frappe.set_route('Form', 'Trademark Classes', d.classes_id)
	}
})