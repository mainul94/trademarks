# -*- coding: utf-8 -*-
# Copyright (c) 2018, mainulkhan94@gmail.com and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe

from frappe.utils import flt, getdate, get_url, now
from frappe import _

from frappe.model.document import Document
from erpnext.controllers.queries import get_filters_cond
from frappe.desk.reportview import get_match_cond
from trademarks.trademarks.doctype.require_document_setup.require_document_setup import get_documents
from frappe.contacts.doctype.address.address import get_default_address

class Trademark(Document):
	def get_feed(self):
		return '{0}: {1}'.format(_(self.status), self.project_name)

	def onload(self):
		"""Load Trademark Classes for quick view"""
		if not self.get('__unsaved') and not self.get("classes"):
			self.load_classes()

		self.set_onload('activity_summary', frappe.db.sql('''select activity_type,
			sum(hours) as total_hours
			from `tabTimesheet Detail` where project=%s and docstatus < 2 group by activity_type
			order by total_hours desc''', self.name, as_dict=True))

		# self.update_costing()

	def __setup__(self):
		self.onload()

	def load_classes(self):
		"""Load `classes` from the database"""
		self.classes = []
		for _class in self.get_classes():
			_class_map = {}
			for x in self.get_child_fields():
				_class_map[x] =  _class.get(x)

			self.map_custom_fields(_class, _class_map)
			_class_map['classes_id'] = _class.name
			self.append("classes", _class_map)

	def get_classes(self):
		if self.name is None:
			return {}
		else:
			filters = {"trademark": self.name}

			if self.get("deleted_classes_list"):
				filters.update({
					'name': ("not in", self.deleted_classes_list)
				})

			return frappe.get_all("Trademark Classes", "*", filters, order_by='idx asc')

	def validate(self):
		self.validate_dates()
		self.sync_classes()
		self.set('classes', [])
		self.update_report_image()
		# self.load_classes()
		# self.send_welcome_email()

	def update_report_image(self):
		for field in ['trademark_image_4', 'trademark_image_3','trademark_image_2', 'trademark_image_1']:
			if self.get(field):
				self.set('image', self.get(field))
				break

	def validate_dates(self):
		if self.expected_start_date and self.expected_end_date:
			if getdate(self.expected_end_date) < getdate(self.expected_start_date):
				frappe.throw(_("Expected End Date can not be less than Expected Start Date"))

	@staticmethod
	def get_exclude_fieldtype():
		return ["Button", "Column Break",
			"Section Break", "Table", "Read Only", "Attach", "Attach Image", "Color", "Geolocation", "HTML", "Image"]

	@staticmethod
	def get_child_fields():
		exclude_fieldtype = Trademark.get_exclude_fieldtype()
		fields = [x.fieldname for x in frappe.get_meta('Trademark Child Classes').fields or [] if x.fieldtype not in exclude_fieldtype] #$["trademark_class", "trademark_class_description","classes_id"]

		custom_fields = frappe.get_all("Custom Field", {"dt": "Trademark Child Classes",
			"fieldtype": ("not in", exclude_fieldtype)}, "fieldname")

		for d in custom_fields:
			fields.append(d.fieldname)
		return fields

	def sync_classes(self):
		"""sync classes and remove table"""
		if not hasattr(self, "deleted_classes_list"):
			self.set("deleted_classes_list", [])

		if self.flags.dont_sync_classes: return
		classes_names = []

		existing_classes_data = {}

		exclude_fieldtype = self.get_exclude_fieldtype()
		fields = self.get_child_fields()

		for d in frappe.get_all('Trademark Child Classes',
			fields = fields,
			filters = {'parent': self.name}):
			existing_classes_data.setdefault(d.classes_id, d)

		for c in self.classes:
			if c.classes_id:
				classes = frappe.get_doc("Trademark Classes", c.classes_id)
			else:
				classes = frappe.new_doc("Trademark Classes")
				classes.trademark = self.name
				if self.customer:
					total_trademarks = frappe.db.count(self.doctype, {'customer': self.customer})
					if self.is_new():
						total_trademarks += 1
					classes.trademarks = total_trademarks
			classes.customer = self.customer
			classes.trademark_obj = self
			if not c.classes_id or self.is_row_updated(c, existing_classes_data, fields):
				for x in fields:
					classes.set(x, c.get(x))
				classes.update({
					"project_name": self.project_name,
					"idx": c.idx
				})

				self.map_custom_fields(c, classes)

				classes.flags.ignore_links = True
				classes.flags.from_trademark = True
				classes.flags.ignore_feed = True

				if c.classes_id:
					classes.update({
						"modified_by": frappe.session.user,
						"modified": now()
					})

					classes.run_method("validate")
					classes.db_update()
				else:
					classes.save(ignore_permissions = True)
				classes_names.append(classes.name)
			else:
				classes_names.append(classes.name)

		# delete
		for c in frappe.get_all("Trademark Classes", ["name"], {"trademark": self.name, "name": ("not in", classes_names)}):
			self.deleted_classes_list.append(c.name)

	@property
	def get_documents(self):
		return get_documents(self.region, self.company_type)

	@property
	def get_existing_documents(self):
		return [x.document for x in self.documents or []]

	@property
	def get_address(self):
		if hasattr(self, 'customer_address') and self.customer_address:
			address = self.customer_address
		elif self.customer:
			address = get_default_address("Customer", self.customer)
		if address:
			return frappe.get_doc('Address', address).as_dict()
		return
	def update_costing_and_percentage_complete(self):
		self.update_percent_complete()
		self.update_costing()

	def is_row_updated(self, row, existing_task_data, fields):
		if self.get("__islocal") or not existing_task_data: return True

		d = existing_task_data.get(row.classes_id)

		for field in fields:
			if row.get(field) != d.get(field):
				return True

	def map_custom_fields(self, source, target):
		trademark_classes_custom_fields = frappe.get_all("Custom Field", {"dt": "Trademark Child Classes"}, "fieldname")

		for field in trademark_classes_custom_fields:
			target.update({
				field.fieldname: source.get(field.fieldname)
			})

	def update_project(self):
		self.update_percent_complete()
		self.update_costing()
		self.flags.dont_sync_classes = True
		self.save(ignore_permissions = True)

	def after_insert(self):
		if self.sales_order:
			frappe.db.set_value("Sales Order", self.sales_order, "trademark_project", self.name)


	def update_percent_complete(self):
		total = frappe.db.sql("""select count(name) from `tabTask` where trademark_project=%s""", self.name)[0][0]
		if not total and self.percent_complete:
			self.percent_complete = 0
		if (self.percent_complete_method == "Task Completion" and total > 0) or (not self.percent_complete_method and total > 0):
			completed = frappe.db.sql("""select count(name) from `tabTask` where
				trademark_project=%s and status in ('Closed', 'Cancelled')""", self.name)[0][0]
			self.percent_complete = flt(flt(completed) / total * 100, 2)

		if (self.percent_complete_method == "Task Progress" and total > 0):
			progress = frappe.db.sql("""select sum(progress) from `tabTask` where
				trademark_project=%s""", self.name)[0][0]
			self.percent_complete = flt(flt(progress) / total, 2)

		if (self.percent_complete_method == "Task Weight" and total > 0):
			weight_sum = frappe.db.sql("""select sum(task_weight) from `tabTask` where
				trademark_project=%s""", self.name)[0][0]
			if weight_sum == 1:
				weighted_progress = frappe.db.sql("""select progress,task_weight from `tabTask` where
					trademark_project=%s""", self.name,as_dict=1)
				pct_complete=0
				for row in weighted_progress:
					pct_complete += row["progress"] * row["task_weight"]
				self.percent_complete = flt(flt(pct_complete), 2)

	def update_costing(self):
		from_time_sheet = frappe.db.sql("""select
			sum(costing_amount) as costing_amount,
			sum(billing_amount) as billing_amount,
			min(from_time) as start_date,
			max(to_time) as end_date,
			sum(hours) as time
			from `tabTimesheet Detail` where project = %s and docstatus = 1""", self.name, as_dict=1)[0]

		from_expense_claim = frappe.db.sql("""select
			sum(total_sanctioned_amount) as total_sanctioned_amount
			from `tabExpense Claim` where trademark_project = %s and approval_status='Approved'
			and docstatus = 1""",
			self.name, as_dict=1)[0]

		self.actual_start_date = from_time_sheet.start_date
		self.actual_end_date = from_time_sheet.end_date

		self.total_costing_amount = from_time_sheet.costing_amount
		self.total_billable_amount = from_time_sheet.billing_amount
		self.actual_time = from_time_sheet.time

		self.total_expense_claim = from_expense_claim.total_sanctioned_amount
		self.update_purchase_costing()
		self.update_sales_amount()
		self.update_billed_amount()

		self.gross_margin = flt(self.total_billed_amount) - (flt(self.total_costing_amount) + flt(self.total_expense_claim) + flt(self.total_purchase_cost))

		if self.total_billed_amount:
			self.per_gross_margin = (self.gross_margin / flt(self.total_billed_amount)) *100

	def update_purchase_costing(self):
		total_purchase_cost = frappe.db.sql("""select sum(base_net_amount)
			from `tabPurchase Invoice Item` where trademark_project = %s and docstatus=1""", self.name)

		self.total_purchase_cost = total_purchase_cost and total_purchase_cost[0][0] or 0

	def update_sales_amount(self):
		total_sales_amount = frappe.db.sql("""select sum(base_grand_total)
			from `tabSales Order` where trademark_project = %s and docstatus=1""", self.name)

		self.total_sales_amount = total_sales_amount and total_sales_amount[0][0] or 0
	#NM
	def update_billed_amount(self):
		total_billed_amount = frappe.db.sql("""select sum(base_grand_total)
			from `tabSales Invoice` where trademark_project = %s and docstatus=1""", self.name)

		self.total_billed_amount = total_billed_amount and total_billed_amount[0][0] or 0

	def after_rename(self, old_name, new_name, merge=False):
		if old_name == self.copied_from:
			frappe.db.set_value('Trademark', new_name, 'copied_from', new_name)

	def send_welcome_email(self):
		url = get_url("/trademark/?name={0}".format(self.name))
		messages = (
		_("You have been invited to collaborate on the project: {0}".format(self.name)),
		url,
		_("Join")
		)

		content = """
		<p>{0}.</p>
		<p><a href="{1}">{2}</a></p>
		"""

		for user in self.users:
			if user.welcome_email_sent==0:
				frappe.sendmail(user.user, subject=_("Project Collaboration Invitation"), content=content.format(*messages))
				user.welcome_email_sent=1

	def on_update(self):
		self.delete_classes()
		self.load_classes()
		# self.update_costing_and_percentage_complete()
		# self.update_dependencies_on_duplicated_project()

	def delete_classes(self):
		if not self.get('deleted_classes_list'): return

		for d in self.get('deleted_classes_list'):
			frappe.delete_doc("Trademark Classes", d)

		self.deleted_classes_list = []

	def update_dependencies_on_duplicated_project(self):
		if self.flags.dont_sync_classes: return
		if not self.copied_from:
			self.copied_from = self.name

		if self.name != self.copied_from and self.get('__unsaved'):
			# duplicated project
			dependency_map = {}
			for task in self.classes:
				_task = frappe.db.get_value(
					'Task',
					{"subject": task.title, "trademark_project": self.copied_from},
					['name', 'depends_on_classes'],
					as_dict=True
				)

				if _task is None:
					continue

				name = _task.name

				dependency_map[task.title] = [ x['subject'] for x in frappe.get_list(
					'Task Depends On', {"parent": name}, ['subject'])]

			for key, value in dependency_map.iteritems():
				task_name = frappe.db.get_value('Task', {"subject": key, "trademark_project": self.name })
				task_doc = frappe.get_doc('Task', task_name)

				for dt in value:
					dt_name = frappe.db.get_value('Task', {"subject": dt, "trademark_project": self.name })
					task_doc.append('depends_on', {"Task": dt_name})

				task_doc.db_update()

#NM
def get_timeline_data(doctype, name):
	'''Return timeline for attendance'''
	return dict(frappe.db.sql('''select unix_timestamp(from_time), count(*)
		from `tabTimesheet Detail` where project=%s
			and from_time > date_sub(curdate(), interval 1 year)
			and docstatus < 2
			group by date(from_time)''', name))

def get_project_list(doctype, txt, filters, limit_start, limit_page_length=20, order_by="modified"):
	return frappe.db.sql('''select distinct project.*
		from tabTrademark project, `tabProject User` project_user
		where
			(project_user.user = %(user)s
			and project_user.parent = project.name)
			or project.owner = %(user)s
			order by project.modified desc
			limit {0}, {1}
		'''.format(limit_start, limit_page_length),
			{'user':frappe.session.user},
			as_dict=True,
			update={'doctype':'Trademark'})

def get_list_context(context=None):
	return {
		"show_sidebar": True,
		"show_search": True,
		'no_breadcrumbs': True,
		"title": _("Trademarks"),
		"get_list": get_project_list,
		"row_template": "templates/includes/projects/project_row.html"
	}

def get_users_for_project(doctype, txt, searchfield, start, page_len, filters):
	conditions = []
	return frappe.db.sql("""select name, concat_ws(' ', first_name, middle_name, last_name)
		from `tabUser`
		where enabled=1
			and name not in ("Guest", "Administrator")
			and ({key} like %(txt)s
				or full_name like %(txt)s)
			{fcond} {mcond}
		order by
			if(locate(%(_txt)s, name), locate(%(_txt)s, name), 99999),
			if(locate(%(_txt)s, full_name), locate(%(_txt)s, full_name), 99999),
			idx desc,
			name, full_name
		limit %(start)s, %(page_len)s""".format(**{
			'key': searchfield,
			'fcond': get_filters_cond(doctype, filters, conditions),
			'mcond': get_match_cond(doctype)
		}), {
			'txt': "%%%s%%" % txt,
			'_txt': txt.replace("%", ""),
			'start': start,
			'page_len': page_len
		})

@frappe.whitelist()
def get_cost_center_name(project):
	return frappe.db.get_value("Trademark", project, "cost_center")
