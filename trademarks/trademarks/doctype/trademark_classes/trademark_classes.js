// Copyright (c) 2018, mainulkhan94@gmail.com and contributors
// For license information, please see license.txt

frappe.ui.form.on('Trademark Classes', {
	setup: function(frm) {
		frm.add_fetch('trademark_class', 'default_description', 'trademark_class_description')
	},
	refresh: function (frm) {
		/**
		 if (frm.doc.trademark) {
			 frappe.model.with_doc('Trademark', frm.doc.trademark, (r)=>{
				 let trademark = frappe.get_doc('Trademark', r)
				 frm.doc.region = trademark.region
				 frm.doc.territory = trademark.territory
				 frm.trigger('set_status_options')
			 })
		 }
		 */
	},
	set_status_options: function (frm) {
		let old_status = frm.doc.class_status || '';
		if (!frm.doc.class_status || frm.doc.__islocal) {
			doctype = 'Trademarks Status'
			filters = { 'is_root': 1 }
		} else {
			doctype = 'Trademarks Status Child'
			filters = { 'parent': frm.doc.class_status }
		}
		if (!frm.doc.region) {
			set_field_options('class_status', [])
			return
		} else {
			filters['territory'] = ['like', '%' + frm.doc.region + '%']
		}
		filters['for_classes'] = 1
		frappe.call({
			method: 'frappe.client.get_list',
			args: {
				doctype: doctype,
				limit_page_length: 0,
				fields: doctype == "Trademarks Status" ? 'name' : 'trademarks_status',
				filters: filters,
				parent: doctype == 'Trademarks Status Child' ? 'Trademarks Status' : '',
				order_by: 'idx asc'
			},
			callback: r => {
				let options = []
				if (r['message']) {
					options = $.map(r.message, function (d) {
						return doctype == 'Trademarks Status' ? d.name : d.trademarks_status
					})
				}
				if (!frm.doc.__islocal)
					options.unshift(old_status);
				set_field_options('class_status', options)
			}
		})
	}
});
