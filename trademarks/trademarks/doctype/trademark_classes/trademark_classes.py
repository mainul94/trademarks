# -*- coding: utf-8 -*-
# Copyright (c) 2018, ahmedragheb75@gmail.com and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class TrademarkClasses(Document):
	def __init__(self, *args, **kwargs):
		super(TrademarkClasses, self).__init__(*args, **kwargs)
		self.trademarks = 0
		self.trademark_obj = None
		self.customer = None

	def validate(self):
		if self.parent and self.parenttype == "Trademark":
			self.set('trademark', self.parent)
		if not self.trademark_class_reference:
			self.set_reference()

	def set_reference(self):
		if not self.trademark:
			return
		if not self.trademark_obj:
			self.trademark_obj = frappe.get_doc('Trademark', self.trademark)
		if not self.trademark_obj:
			return
		if len(self.trademark_obj.classes) > 1 and self.trademark_obj.region == "Hong Kong":
			trademark_class = "HK1%02d" % (len(self.trademark_obj.classes),)
		else:
			trademark_class = self.trademark_class
		customer = self.customer or self.trademark_obj.customer
		if not customer:
			return
		customer = frappe.get_doc('Customer', customer)
		customer_group = frappe.get_doc('Customer Group', customer.customer_group)
		if not self.trademarks:
			self.trademarks = frappe.db.count('Trademark', {'customer': customer.name})
		
		ref = "{}{}{}{}/TM{}{}/S1".format(customer_group.company_id, customer_group.customer_group_year,
		customer_group.customer_group_id, customer.customer_entity_id, self.trademarks, trademark_class)
		self.set('trademark_class_reference', ref)

