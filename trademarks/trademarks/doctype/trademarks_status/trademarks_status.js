// Copyright (c) 2018, mainulkhan94@gmail.com and contributors
// For license information, please see license.txt

frappe.provide('Trademark')

Trademark.TrademarksStatus = frappe.ui.form.Controller.extend({
	setup: function() {
		set_field_options('color', frappe.ui.color.names())
		let me = this
		setTimeout(function(){
			me.setup_awesomplete()
		}, 100)
	},
	setup_awesomplete: function() {
		this.setup_awesomplete_for_input(this.frm.fields_dict.territory.$input.get(0))
	},
	setup_awesomplete_for_input: function(input) {
		function split(val) {
			return val.split( /,\s*/ );
		}
		function extractLast(term) {
			return split(term).pop();
		}

		var awesomplete = new Awesomplete(input, {
			minChars: 0,
			maxItems: 99,
			autoFirst: true,
			list: [],
			item: function(item, input) {
				return $('<li>').text(item.value).get(0);
			},
			filter: function(text, input) { return true },
			replace: function(text) {
				var before = this.input.value.match(/^.+,\s*|/)[0];
				this.input.value = before + text + ", ";
			}
		});
		// That's wrong way as Fixed region.
		// frappe.model.with_doctype("Trademark", function(){ 
		// 	let fields = frappe.get_meta('Trademark').fields
		// 	for (let x=0; x< fields.length; x++) {
		// 		if (fields[x].fieldname === "region") {
		// 			awesomplete.list = fields[x].options.split('\n') || [];
		// 		}
		// 	}
		// })
		var delay_timer;
		var $input = $(input);
		$input.on("input", function(e) {
			clearTimeout(delay_timer);
			delay_timer = setTimeout(function() {
				var term = e.target.value;
				frappe.call({
					method:'trademarks.trademarks.doctype.trademarks_status.trademarks_status.get_territory_list',
					args: {
						'txt': extractLast(term) || '%'
					},
					quiet: true,
					callback: function(r) {
						awesomplete.list = r.message || [];
					}
				});
			},250);
		});
	}
})


cur_frm.cscript = new Trademark.TrademarksStatus({frm: cur_frm})