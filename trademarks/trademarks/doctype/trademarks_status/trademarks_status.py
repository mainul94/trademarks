# -*- coding: utf-8 -*-
# Copyright (c) 2018, mainulkhan94@gmail.com and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class TrademarksStatus(Document):
	def on_update(self):
		self.update_children_tr_values()
	
	def update_children_tr_values(self):
		frappe.db.sql("update `tabTrademarks Status Child` set territory='{}', for_trademark = '{}', for_classes='{}' where trademarks_status='{}'"
		.format(self.territory, self.for_trademark, self.for_classes, self.name))


@frappe.whitelist()
def get_territory_list(txt):
	"""Returns contacts (from autosuggest)"""
	txt = txt.replace('%', '')
	try:
		out = filter(None, frappe.db.sql_list("""select name from `tabTerritory` 
			where name like %(txt)s order by
			if (locate( %(_txt)s, name), locate( %(_txt)s, name), 99999)""",
		        {'txt': "%%%s%%" % frappe.db.escape(txt),
	            '_txt': txt.replace("%", "")
		        })
		)
	except Exception as e:
		raise

	return out
