# -*- coding: utf-8 -*-
# Copyright (c) 2018, mainulkhan94@gmail.com and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class RequireDocumentSetup(Document):
	def autoname(self):
		self.name = ('{}-{}'.format(self.region, self.company_type))

	@property
	def get_documents(self):
		return [x.document for x in self.documents or []]


@frappe.whitelist()
def get_documents(region, company_type):
	if frappe.db.exists('Require Document Setup', {'region': region, 'company_type': company_type}):
		doc = frappe.get_doc('Require Document Setup', {'region': region, 'company_type': company_type})
		return doc.get_documents
	return []
