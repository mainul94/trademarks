frappe.pages['trademark-map'].on_page_load = function (wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Trademark map',
		single_column: true
	});

	wrapper.map = new TrademarkMap({
		page: page,
		wrapper: page.main
	})

}

class TrademarkMap {
	constructor(args) {
		this.maps = false
		this.fields_dict = {}
		this.require_libs = []
		this.data = []
		this.get_require_libs()
		if (args) {
			$.extend(this, args)
		}
		this.make()
	}
	make() {
		let me = this;
		this.$child_wrap = $(`<div style="position:relative"></div>`).appendTo(this.page.main)
		this.$map = $(`<div></div>`).appendTo(this.$child_wrap)
		this.$info_area = $(`<ul class="list-group" style="position: absolute; bottom:5%; left: 5%; width: 250px" >`).appendTo(this.$child_wrap)
		if (this.require_libs && this.require_libs.length) {
			frappe.require(this.require_libs, function () {
				me.setup_filed()
				if (!me.chart) {
					me.init_map()
				}
			})
		}
	}
	setup_filed() {
		let me = this;
		let fields = [
			{
				fieldtype: "Link",
				options: "Customer Group",
				fieldname: "customer_group",
				label: __("Customer Group")
			},
			{
				fieldtype: "Link",
				options: "Customer",
				fieldname: "customer",
				label: __("Customer"),
				get_query: function () {
					let filters = {}
					if (me.fields_dict.customer_group.get_value()) {
						filters['customer_group'] = me.fields_dict.customer_group.get_value()
					}
					return {
						filters: filters,
						query: 'erpnext.controllers.queries.customer_query'
					}
				}
			},
			{
				fieldtype: "Link",
				options: "Trademark",
				fieldname: "trademark",
				label: __("Trademark"),
				get_query: function () {
					let filters = {}
					if (me.fields_dict.customer.get_value()) {
						filters['customer'] = me.fields_dict.customer.get_value()
					} else if (me.fields_dict.customer_group.get_value()) {
						filters['customer_group'] = me.fields_dict.customer_group.get_value()
					}
					return {
						filters: filters,
						query: "trademarks.utils.trademarks_map.get_trademark"
					}
				}
			},
			{
				fieldtype: "Link",
				options: "Trademark Classes Template",
				fieldname: "trademark_class",
				label: __("Classes"),
				get_query: function () {
					let filters = {
						class_status: "Registered"
					}
					if (me.fields_dict.trademark.get_value()) {
						filters['trademark'] = me.fields_dict.trademark.get_value()
					} else if (me.fields_dict.customer.get_value()) {
						filters['customer'] = me.fields_dict.customer.get_value()
					} else if (me.fields_dict.customer_group.get_value()) {
						filters['customer_group'] = me.fields_dict.customer_group.get_value()
					}
					return {
						filters: filters,
						query: "trademarks.utils.trademarks_map.get_trademark_class"
					}
				}
			},
			{
				fieldtype: "Button",
				fieldname: "show_map",
				label: __("Show"),
				click: function () {
					me.show_map(me)
				}
			}
		]
		fields.forEach(function (field) {
			me.fields_dict[field.fieldname] = me.page.add_field(field)
		})
	}
	get_filters_value() {
		let me = this;
		let values = {}
		Object.keys(this.fields_dict).forEach(function (f) {
			if (me.fields_dict[f].get_value())
				values[f] = me.fields_dict[f].get_value();
		})
		return values
	}
	show_map(me) {
		if (me.chart) {
			me.update_cart_data(function () {
				me.chart.draw(me.get_chart_data(), me.get_cart_options())
			})
		}
	}
	init_map() {
		let me = this;
		google.charts.load('current', {
			'packages': ['geochart'],
			'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
		});
		google.charts.setOnLoadCallback(drawRegionsMap);

		function drawRegionsMap() {
			me.chart = new google.visualization.GeoChart(me.$map[0]);
		}
	}
	get_cart_options() {
		return {
			colorAxis: {colors: ['#109618', '#006b06']}
		}
	}
	get_header_data() {
		return [['Country', 'Trademarks']]
	}
	get_chart_data() {
		return google.visualization.arrayToDataTable(this.get_header_data().concat(this.data))
	}
	update_cart_data(callback) {
		let me = this;
		frappe.call({
			method: "trademarks.utils.trademarks_map.get_data",
			args: {
				filters: me.get_filters_value()
			},
			callback: function (r) {
				me.data = []
				if (r['message']) {
					me.data = r.message
				}
				me.update_chart_info()
				if (callback) {
					callback()
				}
			}
		})
	}
	update_chart_info() {
		let me = this;
		this.$info_area.html('')
		this.data.forEach(function(d){
			me.$info_area.append(`<li class="list-group-item row"><strong class="col-xs-9"> ${d[0]}</strong> <span class="col-xs-3">${d[1]}</span></li>`)
		})
	}
	get_require_libs() {
		this.require_libs.push("assets/trademarks/js/google_loader.js")
	}
}