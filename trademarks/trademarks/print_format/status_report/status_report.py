import frappe


def status_data(self, group=None):
	if not group:
		group = self.name
	rows = frappe._dict()
	rows[group] = frappe._dict()
	data = frappe.db.sql("""select trm.customer, cust.customer_name, trm.project_name, trm.image, trm.region, cls.* from tabTrademark as trm
	right join `tabTrademark Classes` as cls on trm.name = cls.trademark
	right join `tabCustomer` as cust on trm.customer = cust.name
	where trm.region is not null and cls.trademark is not null and cust.customer_group = '{}'""".format(group), as_dict=True)
	# frappe.msgprint(str(data))
	for d in data:
		if d.customer and not rows[group].get(d.customer):
			rows[group][d.customer] = frappe._dict({"customer_name": d.customer_name, "region": frappe._dict()})
		if d.region and not rows[group][d.customer].region.get(d.region):
			rows[group][d.customer].region[d.region] = frappe._dict({"region_name": d.region, "trademarks": frappe._dict()})
		if d.trademark and not rows[group][d.customer].region[d.region].trademarks.get(d.trademark):
			rows[group][d.customer].region[d.region].trademarks[d.trademark] = frappe._dict({'project_name': d.project_name, 'image': d.image, 'filings': frappe._dict()})
		fill_key = d.trademark_class_filing_number or "no_filling"
		if fill_key and not rows[group][d.customer].region[d.region].trademarks[d.trademark].filings.get(fill_key):
			rows[group][d.customer].region[d.region].trademarks[d.trademark].filings[fill_key] = frappe._dict({
				"filing_number": d.trademark_class_filing_number,
				"class_reference": d.trademark_class_reference,
				"filing_date": d.trademark_class_filing_date,
				"class_status": [],
				"classes": []
			})
		if d.trademark_class and d.trademark_class not in rows[group][d.customer].region[d.region].trademarks[d.trademark].filings[fill_key]['classes']:
			rows[group][d.customer].region[d.region].trademarks[d.trademark].filings[fill_key]['classes'].append(d.trademark_class)
		
		if d.class_status:
			rows[group][d.customer].region[d.region].trademarks[d.trademark].filings[fill_key]['class_status'].append(d.class_status)
		
		# if d.class_status and  not rows[group][d.customer].region[d.region].trademarks[d.trademark].filings[fill_key]['class_status']:
			# rows[group][d.customer].region[d.region].trademarks[d.trademark].filings[fill_key]['class_status'] = d.class_status
		
	return rows
