from frappe.custom.doctype.custom_field.custom_field import create_custom_fields
fields = {"Customer Group": [{
    "label": "Company ID",
    "fieldname": "company_id",
    "fieldtype": "Select",
    "options": "Select\nB\nC",
    "reqd": 1,
    "in_list_view": 1,
    "in_standard_filter": 1,
    "default": "Select",
    "insert_after": "customer_group_name",
    "print_hide": 0
},
    {
        "label": "Customer Group Year",
        "fieldname": "customer_group_year",
        "fieldtype": "Select",
        "options": "Select\n20\n19\n18\n17\n16\n15\n14\n13",
        "reqd": 1,
        "in_list_view": 1,
        "in_standard_filter": 0,
        "default": "Select",
        "insert_after": "company_id",
        "print_hide": 0
},
    {
        "label": "Customer Group ID",
        "fieldname": "customer_group_id",
        "fieldtype": "Data",
        "reqd": 1,
        "in_list_view": 1,
        "in_standard_filter": 1,
        "insert_after": "customer_group_year",
        "print_hide": 0
}
],
"Customer":[
    {
        "label": "Customer Entity ID in this group",
        "fieldname": "customer_entity_id",
        "fieldtype": "Select",
        "options": "Select\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10",
        "reqd": 1,
        "in_list_view": 1,
        "in_standard_filter": 0,
        "default": "Select",
        "insert_after": "customer_type",
        "print_hide": 0
    }
]}


def execute():
    create_custom_fields(fields)
