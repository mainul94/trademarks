import frappe

def execute():
    frappe.db.sql("""update `tabTrademark` inner join tabCustomer on tabTrademark.customer = tabCustomer.name
    set tabTrademark.company_type = tabCustomer.customer_type where tabTrademark.company_type is null and tabCustomer.customer_type is not null""")
    frappe.db.commit()